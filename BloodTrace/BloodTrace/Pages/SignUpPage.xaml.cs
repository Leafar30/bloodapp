﻿using BloodTrace.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BloodTrace.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SignUpPage : ContentPage
	{
		public SignUpPage ()
		{
			InitializeComponent ();
		}

        private async void BtnSignUp_Clicked(object sender, EventArgs e)
        {
            ApiServices apiServices = new ApiServices();

            bool response = await apiServices.RegisterUser(EntEmail.Text, EntPassword.Text, EntConfirmPassword.Text);
            if (!response)
            {
                await DisplayAlert("Uh-Oh", "Something went wrong", "Cancel");
            }
            else
            {
                await DisplayAlert("Puiaaa", "User has been registered successfully", "Ok");
                await Navigation.PopToRootAsync();
            }

        }
    }
}