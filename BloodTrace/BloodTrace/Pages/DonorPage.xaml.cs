﻿using BloodTrace.Models;
using BloodTrace.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BloodTrace.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DonorPage : ContentPage
	{
        public ObservableCollection<BloodUser> BloodUsers;

        public DonorPage ()
		{
			InitializeComponent ();

            BloodUsers = new ObservableCollection<BloodUser>();
            FindBloodDonar();
        }

        private async void FindBloodDonar()
        {
            ApiServices apiService = new ApiServices();

            var bloodUsers = await apiService.LastestBloodUser();

            foreach (var bloodusers in bloodUsers)
            {
                BloodUsers.Add(bloodusers);
            }

            // LvBloodDonars.ItemsSource = BloodUsers;
            LBloodDonars.ItemsSource = BloodUsers;
        }

        private void LvBloodDonars_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var selectedDonar = e.SelectedItem as BloodUser;
            if (selectedDonar != null)
            {
                Navigation.PushAsync(new DonarProfilePage(selectedDonar));
            }

            ((ListView)sender).SelectedItem = null;
        }
    }
}