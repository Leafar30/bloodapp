﻿using BloodTrace.Models;
using BloodTrace.Services;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BloodTrace.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RegisterBlood : ContentPage
	{

		public MediaFile file;

		public RegisterBlood ()
		{
			InitializeComponent ();
		}

	   

		private async void Button_Clicked(object sender, EventArgs e)
		{
		   //var imagemArray =  FileHelper.ReadFully(file.GetStream());
		   //file.Dispose();
		   var country = PickerCountry.Items[PickerCountry.SelectedIndex];
		   var bloodGroup = PickerBloodGroup.Items[PickerBloodGroup.SelectedIndex];

			DateTime dateTime = DateTime.Now;
			int d = Convert.ToInt32(dateTime.ToOADate());

			var bloodUser = new BloodUser()
			{
				UserName = EntName.Text,
				Email = EntEmail.Text,
				Phone = EntPhone.Text,
				BloodGroup = bloodGroup,
				Country = country,
				Date = d,
			   // ImageArray = imagemArray,
			};

			
				ApiServices apiServices = new ApiServices();
				var response = await apiServices.RegisterDonar(bloodUser);

				if (!response)
				{
					await DisplayAlert("Error", "Something went wrong", "Cancel");
				}
				else
				{
					await DisplayAlert("Got it!", "Your registration has been completed", "Close");
				}

			
			
		}

		private async void TapOnCamera_Tapped(object sender, EventArgs e)
		{
			await CrossMedia.Current.Initialize();

			if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
			{
				await DisplayAlert("No Camera", ":( No camera available.", "OK");
				return;
			}

			file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
			{
				Directory = "Sample",
				Name = "test.jpg"
			});

			if (file == null)
				return;

			await DisplayAlert("File Location", file.Path, "OK");

			ImgDonar.Source = ImageSource.FromStream(() =>
			{
				var stream = file.GetStream();
				return stream;
			});
		}
	}
}